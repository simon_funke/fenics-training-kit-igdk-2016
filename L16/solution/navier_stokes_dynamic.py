""" This program optimises the control of the Navier-Stokes equation """
from fenics import *
from mshr import *
from dolfin_adjoint import *

set_log_level(ERROR)

# Create a rectangle with a circular hole.
rect = Rectangle(Point(0.0, 0.0), Point(30.0, 10.0))
circ = Circle(Point(10, 5), 2.5)
domain = rect - circ
N = 120  # Mesh resolution
mesh = generate_mesh(domain, N)
plot(mesh)

# Define function spaces (P2-P1)
V = VectorFunctionSpace(mesh, "Lagrange", 2)  # Velocity
Q = FunctionSpace(mesh, "Lagrange", 1)        # Pressure
D = FunctionSpace(mesh, "DG", 1)        # Control space
W = MixedFunctionSpace([V, Q])

# Define test and solution functions
v, q = TestFunctions(W)
s = Function(W, name="state")
u, p = split(s)

s_old = Function(W, name="old_state")
u_old, p_old = split(s)

# Set parameter values
nu = Constant(1.0)     # Viscosity coefficient
f = Function(D, name="Control")      # Control

# Time discretization
timestep = 0.05
t = 0.0
T = 1.0

# Define boundary conditions
noslip = DirichletBC(W.sub(0), (0, 0), "on_boundary && x[0] > 0.0 && x[0] < 30")
p0 = Expression("sin(2*pi*t)*a*(30.0 - x[0])/30.0", degree=1, a=1.0, t=t)
bcu = [noslip]
n = FacetNormal(mesh)

# Define the indicator function for the control area
xcoor = SpatialCoordinate(mesh)
chi = conditional(xcoor[1] >= 5, 1, 0)


# Define the variational formulation of the Navier-Stokes equations
F =(inner(u-u_old, v)/Constant(timestep)*dx + # Time derivative
     inner(grad(u)*u, v)*dx +                 # Advection term
     nu*inner(grad(u), grad(v))*dx -          # Diffusion term
     inner(p, div(v))*dx +                    # Pressure term
     inner(chi*f*u, v)*dx +                   # Sponge term
     div(u)*q*dx +                            # Divergence term
     p0*dot(v, n)*ds
)

# Run the forward model
adj_start_timestep(t)
while t+timestep <= T:
    s_old.assign(s)
    t += timestep
    p0.t = t
    # Solve the Navier-Stokes equations
    solve(F == 0, s, bcs=bcu)
    adj_inc_timestep(time=t, finished=t>T)

# Visualise forward as HTML webpage
adj_html("forward.html", "forward")
adj_html("adjoint.html", "adjoint")

# Define the optimisation proble,
alpha = Constant(1.0e-2)
J = Functional(inner(grad(u), grad(u))*dx*dt + alpha*inner(f,f)*dx*dt[START_TIME])
m = Control(f)
R = ReducedFunctional(J, m)

# Solve the optimisation problem
m_opt = minimize(R, method="L-BFGS-B", tol=1.0e-5)

# Plot the optimised results
f.assign(m_opt)
s.assign(Function(W))

# Store results into files
File("solution/ctrl_opt.pvd") << m_opt
u_file = File("solution/u_opt.pvd")
p_file = File("solution/p_opt.pvd")

t = 0.0
u, p = s.split(deepcopy=True)
u_file << (u, t)
p_file << (p, t)
while t+timestep <= T:
    s_old.assign(s)
    t += timestep
    p0.t = t
    # Solve the Navier-Stokes equations
    solve(F == 0, s, bcs=bcu)
    u, p = s.split(deepcopy=True)
    u_file << (u, t)
    p_file << (p, t)
    print "t", t
