from dolfin import *
from petsc4py import *
import numpy

# Use agressive compiler optimisation flags
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

N =  8
dim = 3
domain = [UnitIntervalMesh, UnitSquareMesh, UnitCubeMesh][dim-1]
mesh = domain(*((N,)*dim))

# Taylor-Hood elements
P2 = VectorElement("CG", mesh.ufl_cell(), 2)
P1 = FiniteElement("CG", mesh.ufl_cell(), 1)
TH = MixedElement([P2, P1])

W = FunctionSpace(mesh, TH)

#bcs = DirichletBC(W.sub(0),
#                 Expression(("4*x[1]*(1-x[1])",) +  ("0.",)*(dim-1), degree=2),
#                 "(x[0]<1-DOLFIN_EPS)&&on_boundary")
bc1 = DirichletBC(W.sub(0),
                 Expression(("8*x[1]*(1-x[1])*x[2]*(1-x[2])",) +  ("0.",)*(dim-1), degree=2),
                 "(x[0]<1-DOLFIN_EPS)&&on_boundary")

bc2 = DirichletBC(W.sub(0), Constant((0, 0, 0)), "on_boundary && x[0] > DOLFIN_EPS && x[0] < 1- DOLFIN_EPS")
bcs = [bc1, bc2]


u, p = TrialFunctions(W)
v, q = TestFunctions(W)

# Stokes equations in variational form
nu = Constant(0.001)
a = (nu*inner(grad(u), grad(v)) - p * div(v) - q * div(u)) * dx

f = Constant((0,)*dim)
L = inner(f, v) * dx

# Assemble and apply BC
timer = Timer("FEniCS assembly")
A, b = assemble_system(a, L, bcs)
timer.stop()

# Check that system is symmetric (only works in serial)
#assert max(sum(abs(A.array().transpose() - A.array()))) < DOLFIN_EPS


# Assemble a preconditioner
m = nu*inner(grad(u), grad(v))*dx + p*q*dx
(M, _) = assemble_system(m, L, bcs)

# Define nullspace
#null_vec = Function(W).vector()
#null_vec[W.sub(1).dofmap().dofs()] = 1.
#null_vec[:] *= 1./null_vec.norm("l2")
#nullspace = VectorSpaceBasis([null_vec])
#A.set_nullspace(nullspace)

# fenics direct solve
solver = LUSolver('mumps')
solver.set_operator(A)
solver.parameters["symmetric"] = True
U = Function(W)
timer = Timer("FEniCS direct solve with MUMPS")
solver.solve(U.vector(), b)
timer.stop()

# fenics iterative solve
solver = KrylovSolver("tfqmr", "amg")
solver.set_operators(A, M)
U1 = Function(W)
timer = Timer("FEniCS solve with TFQMR and AMG")
solver.solve(U1.vector(), b)
timer.stop()
print "Diff direct vs iterative: ", numpy.max(numpy.abs(U.vector().array()-U1.vector().array()))

# export to petsc4py
A = as_backend_type(A).mat()
b = as_backend_type(b).vec()
M = as_backend_type(M).mat()
s = Function(W)
s_ = as_backend_type(s.vector()).vec()


# Outer solver settings
#PETScOptions.set("ksp_view")     # Print out all settings
PETScOptions.set("ksp_monitor")  # Monitor convergence
PETScOptions.set("ksp_converged_reason")  # Print out convergence reason
PETScOptions.set("ksp_type", "fgmres") # Alternative: TFQMR
PETScOptions.set("ksp_rtol", 1e-10)


# Fieldsplit settings
PETScOptions.set("pc_type", "fieldsplit")
PETScOptions.set("pc_fieldsplit_type", "schur")
PETScOptions.set("pc_fieldsplit_schur_factorization_type", "diag")
PETScOptions.set("pc_fieldsplit_schur_precondition", "full")

# Solver for the (1,1) block
PETScOptions.set("fieldsplit_velocity_ksp_type", "preonly")
PETScOptions.set("fieldsplit_velocity_pc_type", "lu")
PETScOptions.set("fieldsplit_velocity_pc_hypre_type", "boomeramg")

# Solver for the (2,2) block
PETScOptions.set("fieldsplit_pressure_ksp_monitor")
PETScOptions.set("fieldsplit_pressure_ksp_type", "preonly")
PETScOptions.set("fieldsplit_pressure_pc_type", "lu")
PETScOptions.set("fieldsplit_pressure_ksp_max_it",  4)

# fieldsplit solve
ksp = PETSc.KSP().create()
ksp.setFromOptions()

pc = ksp.getPC()
is0 = PETSc.IS().createGeneral(W.sub(0).dofmap().dofs())
is1 = PETSc.IS().createGeneral(W.sub(1).dofmap().dofs())
pc.setFieldSplitIS(('velocity', is0), ('pressure', is1))

ksp.setOperators(A, A)

timer = Timer("FEniCS PETSc solver")
ksp.solve(b, s_)
timer.stop()
print "Diff iterative vs PETSc solver", (numpy.max(numpy.abs(U1.vector().array()-s_.array)))

File("u.pvd") << U.split()[0]
File("p.pvd") << U.split()[1]
File("u1.pvd") << U.split()[0]
File("p1.pvd") << U.split()[1]

list_timings(True, [TimingType_wall])
