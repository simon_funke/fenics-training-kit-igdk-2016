FEniCS
======

5-day course, 04 - 08 July 2016

At Technical University of Munich, Mathematical Institute

http://igdk1754.ma.tum.de/IGDK1754/CCFunke2016

Course content
--------------

The FEniCS Project is a collection of open source software for the automated,
efficient solution of partial differential equations. The software allows
specifying finite element variational problems in close to mathematical form
and, via automated code generation techniques, the automated assembly and
solution of such. This is a powerful and exciting combination that enables
rapid, reliable and fun development of efficient finite element models. In
addition, the FEniCS extension dolfin-adjoint automates adjoint model
implementation and provides an environment for rapidly solving PDE-constrained
optimization problems.

This five-day course will consist of short lectures in combination with
hands-on exercises aimed at novice and intermediate FEniCS users: starting from
the very basics of the Finite Element method, to solving non-trivial,
nonlinear, time-dependent PDEs, all the way to solving advanced, time-dependent
PDE-constrained optimization problems.

Topics covered in the course include: solving linear static PDEs, solving
nonlinear static PDEs, solving linear time-dependent PDEs, mixed problems,
splitting methods, discontinuous Galerkin methods, adjoint methods and solving
PDE-constrained optimization problems. Partial differential equations solved in
the course include the Poisson equation, a nonlinear Poisson equation, the
Stokes equations, nonlinear hyperelasticity (St. Venant–Kirchhoff), and the
optimal control of the incompressible Navier-Stokes equations.

Requirements
------------

### Software requirements

Please bring your own laptop. The course will be based on

* FEniCS (www.fenicsproject.org)
* dolfin-adjoint (www.dolfin-adjoint.org)

We will also use

* Jupyter Notebook (www.jupyter.org)
* SciPy (www.scipy.org)
* Moola (https://github.com/funsim/moola)

We will use the 2016.1 version of the FEniCS and dolfin-adjoint software.

### Installation instructions

We will devote one session to installation, so please feel free to arrive at the course without FEniCS preinstalled.

Installation instructions for FEniCS are available here: http://fenicsproject.org/download/.

As you can see, there are many ways to install FEniCS -- we'll use the Docker approach:

http://fenicsproject.org/download/#docker-images-all-platforms-and-versions-hspace-dockerimage

How to install and run FEniCS for the IGDK Compact course:

* Go to the "Docker images" section of fenicsproject.org/download
* Follow - Step 1: Install Docker, including testing your installation - Step 2: Install the 'fenicsproject' script
* Run (in your terminal/Docker Terminal for Mac/Windows) `$ fenicsproject help`
* Open a new "FEniCS" project, by running `$ fenicsproject notebook igdk2016 quay.io/dolfinadjoint/dolfin-adjoint` NB: This may take some time (depending on the internet connection) This command will create a persistent FEniCS session, named 'igdk2016', using the 2016 version of FEniCS with dolfin-adjoint, and with the Jupyter notebook.
* Start your FEniCS session with `$ fenicsproject start igdk2016`. This should give you a message like "You can access the Jupyter notebook at http://localhost:3000". Point a webbrowser at that location.
* Voila, a Jupyter session should have appeared
* Open a new Notebook by pressing the New -> Notebook (Python 2)
* Test your FEniCS installation by entering this in a Cell `from dolfin import *` and `print dolfin.__version__` - and execute. The output should read: 2016.1.0
* Test your installation further by running the `fenics_test.ipynb` notebook located [here](https://bitbucket.org/simon_funke/fenics-training-kit-igdk-2016/raw/master/fenics_test.ipynb).

Required knowledge
------------------

* Knowledge of partial differential equations
* Knowledge of numerical simulation of differential equations
* Basic knowledge of Python

Training team
-------------

Trainer: Dr. Simon W. Funke

Infrastructure
--------------

Slack channel "fenics"
